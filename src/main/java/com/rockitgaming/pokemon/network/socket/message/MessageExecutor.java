package com.rockitgaming.pokemon.network.socket.message;

import com.rockitgaming.pokemon.network.domain.Message;
import com.rockitgaming.pokemon.network.domain.Session;

public interface MessageExecutor {

    void execute(Session session, Message message);
}
