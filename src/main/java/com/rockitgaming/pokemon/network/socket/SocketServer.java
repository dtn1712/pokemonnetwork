package com.rockitgaming.pokemon.network.socket;


import com.rockitgaming.pokemon.network.Constants;
import com.rockitgaming.pokemon.network.ServerConfig;
import com.rockitgaming.pokemon.network.codec.MessageDecoder;
import com.rockitgaming.pokemon.network.codec.MessageEncoder;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.timeout.IdleStateHandler;
import io.netty.util.concurrent.GenericFutureListener;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.net.SocketAddress;

@Component
public class SocketServer {

	private static final Logger logger = LogManager.getLogger(SocketServer.class);

	@Autowired
	private SocketServerHandler socketServerHandler;

	public void start() throws InterruptedException {

		logger.info("====================== PREPARE SERVER ======================= ");
		EventLoopGroup bossGroup = new NioEventLoopGroup();
		EventLoopGroup workerGroup = new NioEventLoopGroup();
		try {
			ServerBootstrap bootstrap = new ServerBootstrap();
			bootstrap.group(bossGroup, workerGroup).channel(NioServerSocketChannel.class).childHandler(new ChannelInitializer<SocketChannel>() {

				@Override
				protected void initChannel(SocketChannel ch) throws Exception {
					ch.pipeline().addLast(new LoggingHandler(LogLevel.INFO));
					ch.pipeline().addLast(new IdleStateHandler(Constants.MAX_READ_IDLE_SECONDS,Constants.MAX_WRITE_IDLE_SECONDS,Constants.MAX_ALL_IDLE_SECONDS));
					ch.pipeline().addLast(new MessageDecoder());
					ch.pipeline().addLast(new MessageEncoder());
					ch.pipeline().addLast(socketServerHandler);
				}

			}).childOption(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT)
					.option(ChannelOption.SO_BACKLOG, ServerConfig.soBackLog)
					.option(ChannelOption.CONNECT_TIMEOUT_MILLIS, ServerConfig.connectTimeoutMillis)
					.childOption(ChannelOption.SO_KEEPALIVE, ServerConfig.soKeepAlive);

			ChannelFuture future = bootstrap.bind(ServerConfig.serverIp, ServerConfig.serverPort).sync();
			future.addListener(new GenericFutureListener<ChannelFuture>() {
				@Override
				public void operationComplete(ChannelFuture future) throws Exception {
					if (future.isSuccess()) {
						SocketAddress localAddress = future.channel().localAddress();
						logger.info("SERVER INFO:" + localAddress.toString());
						logger.info("====================== SERVER STARTED =======================");
					} else {
						logger.error("Bound attempt failed! ", future.cause().toString());
					}
				}
			});

			future.channel().closeFuture().sync();

		} finally {
			workerGroup.shutdownGracefully();
			bossGroup.shutdownGracefully();
		}

	}
}
