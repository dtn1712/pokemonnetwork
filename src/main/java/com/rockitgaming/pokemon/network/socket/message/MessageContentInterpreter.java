package com.rockitgaming.pokemon.network.socket.message;


import com.rockitgaming.pokemon.network.util.ConverterUtil;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


public class MessageContentInterpreter implements InvocationHandler{

	private Map<Short, Key> keyInfoMap = new HashMap<>();
	private Map<Short, String> commandMap = new HashMap<>();


	public MessageContentInterpreter(Class<?>... constantClasses) {
		interpreter(constantClasses);
	}


	private void interpreter(Class<?>... constantClasses) {
		for (Class<?> clazz : constantClasses) {
			Object constantObject = Proxy.newProxyInstance(getClass().getClassLoader(), clazz.getInterfaces(), this);
			Field[] fields = clazz.getDeclaredFields();
			Field[] arrayOfField1;
			int j = (arrayOfField1 = fields).length;
			for (int i = 0; i < j; i++) {
				Field field = arrayOfField1[i];
				if (field.getName().startsWith("COMMAND")) {
					extractCommandInfo(constantObject, field);
				}
			}
		}
	}


	/**
	 * Extract thông tin của command
	 *
	 * @param field
	 */
	private void extractCommandInfo(Object constantObject, Field field) {
		try {
			String fieldName = field.getName();
			String name = omitFirstWord(fieldName);
			commandMap.put((Short) field.get(constantObject), name);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	/**
	 * Bỏ đi ký tự "_"
	 *
	 * @param fieldName
	 * @return
	 */
	private String omitFirstWord(String fieldName) {
		String name = fieldName.substring(fieldName.indexOf('_') + 1).toLowerCase();
		name = name.replace("_", " ");
		return name;
	}


	public String interpretValue(Short key, byte[] value) {
		MessageContentInterpreter.Key keyz = this.keyInfoMap.get(key);
		Class<?> clazz;
		if (keyz == null) {
			clazz = byte[].class;
		} else {
			clazz = keyz.clazz;
		}

		Object result;
		try {
			result = ConverterUtil.convertBytes2Object(clazz, value);
		} catch (Exception e) {
			return "<invalid data type> " + new String(value);
		}

		if ((result instanceof byte[])) {
			result = Arrays.toString((byte[]) result);
		}

		if (result == null) {
			return "null ";
		}

		return result.toString();
	}


	public String interpretKey(Short key) {
		MessageContentInterpreter.Key keyz = keyInfoMap.get(key);
		String name;
		if (keyz == null) {
			name = "Unknown";
		} else {
			name = keyz.name;
		}

		return name;
	}


	public String interpretCommand(Short commandId) {
		String name = commandMap.get(commandId);
		return name == null ? "Unknown Command" : name;
	}


	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		return null;
	}

	private class Key {
		String name;
		Class<?> clazz;


		public Key(String name, Class<?> clazz) {
			this.name = name;
			this.clazz = clazz;
		}
	}

}
