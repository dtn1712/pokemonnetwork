package com.rockitgaming.pokemon.network.socket.handler;


import com.rockitgaming.pokemon.network.domain.Message;
import com.rockitgaming.pokemon.network.domain.Session;


public interface RequestHandler {

	void handle(Session session, Message message);
}
