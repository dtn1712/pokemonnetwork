package com.rockitgaming.pokemon.network.socket.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rockitgaming.pokemon.network.domain.Message;
import com.rockitgaming.pokemon.network.domain.Session;
import com.rockitgaming.pokemon.network.socket.SocketErrorCode;
import com.rockitgaming.pokemon.network.socket.message.MessageFactory;
import com.rockitgaming.pokemon.network.util.MapperUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Map;

public abstract class BaseValidateInputRequestHandler extends BaseRequestHandler {

    private static final Logger logger = LogManager.getLogger(BaseValidateInputRequestHandler.class);

    protected List<String> requireFields;

    public BaseValidateInputRequestHandler(List<String> requireFields) {
        this.requireFields = requireFields;
    }

    @Override
    public void handle(Session session, Message message) {
        String messageStringData = message.getString(Message.DATA_KEY);
        if (messageStringData != null) {
            ObjectMapper objectMapper = MapperUtils.getObjectMapper();
            Map<String, Object> messageData;
            try {
                messageData = objectMapper.readValue(messageStringData, Map.class);
            } catch (Exception e) {
                logger.error("Failed read the input data",e);
                writeMessage(session, MessageFactory.createErrorMessage(SocketErrorCode.ERROR_INVALID_INPUT_FORMAT,e.getMessage()));
                return;
            }
            List<String> missingRequiredFields = checkMissingFields(requireFields, messageData);
            if (!missingRequiredFields.isEmpty()) {
                writeMessage(session, MessageFactory.createErrorMessage(SocketErrorCode.ERROR_INVALID_INPUT,
                        String.format("Missing data input %s", missingRequiredFields.toString())));
                return;
            }

            handleRequest(session, message, messageData);
        } else {
            writeMessage(session, MessageFactory.createErrorMessage(SocketErrorCode.ERROR_NO_INPUT,"No input received"));
        }
    }

    protected abstract void handleRequest(Session session, Message message, Map<String, Object> messageData);
}
