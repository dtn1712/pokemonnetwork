package com.rockitgaming.pokemon.network.socket.channel;

import com.rockitgaming.pokemon.network.domain.Session;
import io.netty.channel.Channel;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.AttributeKey;
import io.netty.util.concurrent.GlobalEventExecutor;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class ChannelService {

	private final ChannelGroup channels = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

	private static final AttributeKey<Session> SESSION = AttributeKey.valueOf("dispatcher.session");

	private Map<String, Channel> clientChannels = new ConcurrentHashMap<>();

	public Session connect(Channel channel) {
		String sessionId = UUID.randomUUID().toString();

		Session session = new Session();
		session.setSessionId(sessionId);
		session.setClientIp(channel.remoteAddress().toString().substring(1));
		channel.attr(SESSION).set(session);

		channels.add(channel);
		clientChannels.put(sessionId, channel);

		return session;
	}

	public Channel getChannel(String sessionId) {
		return clientChannels.get(sessionId);
	}

	public String remove(Channel channel) {
		Session session = channel.attr(SESSION).get();
		clientChannels.remove(session.getSessionId());
		channels.remove(channel);
		channel.attr(SESSION).set(null);
		return session.getSessionId();
	}

	public Session getSession(Channel channel) {
		return channel.attr(SESSION).get();
	}

}
