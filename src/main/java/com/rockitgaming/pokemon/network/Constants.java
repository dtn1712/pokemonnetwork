package com.rockitgaming.pokemon.network;


public class Constants {

    public static final int MAX_READ_IDLE_SECONDS = 60;
    public static final int MAX_WRITE_IDLE_SECONDS = 60;
    public static final int MAX_ALL_IDLE_SECONDS = 60;

    public static final long MESSAGE_POLLING_INTERVAL = 100;

}
