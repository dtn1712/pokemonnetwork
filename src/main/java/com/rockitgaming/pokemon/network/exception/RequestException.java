package com.rockitgaming.pokemon.network.exception;


public class RequestException extends RuntimeException{

    public RequestException(String message) {
        super(message);
    }
}
